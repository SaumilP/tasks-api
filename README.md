tasks-api-app
---
Project built using golang and fiber to expose CRUD api(s).

# Setup #
This section indicates required tools.

## Required software / libs / tool
* golang 1.15+
* docker
* vegeta

## configurations ##
* Build and start the app in docker containers
```sh
$> docker build -t task-api-docker .
$> docker-compose up -d 
```

* cleanup
```sh
$> docker-compose down
```